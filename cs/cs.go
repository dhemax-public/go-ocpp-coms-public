package cs

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"runtime"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	"github.com/eduhenke/go-ocpp"
	"github.com/eduhenke/go-ocpp/internal/log"
	"github.com/eduhenke/go-ocpp/internal/service"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpreq"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpresp"
	"github.com/eduhenke/go-ocpp/ws"
)

// ChargePointMessageHandler handles the OCPP messages coming from the charger
type ChargePointMessageHandler func(cprequest cpreq.ChargePointRequest, cpID string) (cpresp.ChargePointResponse, error)

//CentralSystem ...
type CentralSystem interface {
	// Run the central system on the given port
	// and handles each incoming ChargepointRequest
	Run(port string, cphandler ChargePointMessageHandler) error

	// GetServiceOf a chargepoint to enable
	// communication with the chargepoint
	//
	// the url parameter is *NOT* used if
	// the link between the CentralSystem
	// and Chargepoint is via Websocket
	GetServiceOf(cpID string, version ocpp.Version, url string) (service.ChargePoint, error)

	DeleteConnection(cpID string)
}

type centralSystem struct {
	// Mutex *sync.Mutex
	conns map[string]*ws.Conn
	// Mutex sync.Mutex
}

var (
	connections      map[string]*ws.Conn
	connectionsMutex = sync.Mutex{}
)

//New ...
func New() CentralSystem {
	// Mutex sync.Mutex
	return &centralSystem{
		conns: make(map[string]*ws.Conn, 0),
	}
}

//ConnectedMachines ...
var ConnectedMachines []string
var machineLogs = make(map[string][]string)
var MachinesCPid []string
var closeGracePeriod = 1 * time.Second
var writeWait = 2 * time.Second

func GetAuthorizedMachines() {
	MachinesCPid = append(MachinesCPid, "0013")
	MachinesCPid = append(MachinesCPid, "0012")
	fmt.Printf("\nAgregando maquinas de prueba\n")
}

func IsMachineAuthorized(machineConn string) bool {
	// fmt.Printf("\nIngresa maquina %s\n", machineConn)
	for i := range MachinesCPid {
		if MachinesCPid[i] == machineConn {
			fmt.Printf("\nmaquina: %s =? ingresa: %s\n", MachinesCPid[i], machineConn)
			return true
		}
	}
	return false
}

//ReturnLogs ...
func ReturnLogs() map[string][]string {
	// func ReturnLogs() interface{} {
	return machineLogs
}

func ReturnAllLogs() []string {
	// func ReturnAllLogs() interface{} {
	return machineLogs["_all_"]
}

//ContainLogs ...
func ContainLogs(machineID string) bool {
	for i := range machineLogs {
		if i == machineID {
			return true
		}
	}
	return false
}

//AddLog ...
func AddLog(machineID string, text string) {

	// if IsMachineConnect(machineID) {
	// 	if !ContainLogs(machineID) {
	// 		machineLogs[machineID] = []string{text}
	// 	} else {
	// 		machineLogs[machineID] = append(machineLogs[machineID], text)
	// 		// machineLogs[machineID] = append( []string{machineID} , text)
	// 	}
	// } else {
	// 	machineLogs["_all_"] = append(machineLogs[machineID], text)
	// 	// machineLogs["_all_"] = append( []string{machineID}, text)
	// }
}

func EmptyLogs() {
	machineLogs = nil
}

//IsMachineConnect ...
func IsMachineConnect(machineConn string) bool {
	for i := range ConnectedMachines {
		if ConnectedMachines[i] == machineConn {
			return true
		}
	}
	return false
}

//ListMachines ...
func ListMachines() string {
	var ListString string = ""
	for i := range ConnectedMachines {
		ListString += "Machine ID #" + fmt.Sprintf("%v", i+1) + " [" + fmt.Sprintf("%v", ConnectedMachines[i]) + "]\n"
	}
	return ListString
}

//HaveEmpty ...
func HaveEmpty() bool {
	for _, element := range ConnectedMachines {
		if element == " " {
			return true
		}
	}
	return false
}

//RemoveMachine ...
func RemoveMachine(machineID string) {
	for i, v := range ConnectedMachines {
		if v == machineID {
			ConnectedMachines = append(ConnectedMachines[:i], ConnectedMachines[i+1:]...)
		}
	}
	fmt.Printf("\n===[The machine ID %v has been disconnected]===\n", machineID)
}

func (csys *centralSystem) Run(port string, cphandler ChargePointMessageHandler) error {
	log.Debug("== [CENTRAL SYSTEM SERVER ONLINE] ==\n")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if ws.IsWebSocketUpgrade(r) {
			csys.handleWebsocket(w, r, cphandler)
		}
	})

	log.Debug("Central system running on port: [%s]", port)
	return http.ListenAndServe(port, nil)
}

// Agregar o modificar o utilizar para autorizar o no la conexion y guardar en la BD
func (csys *centralSystem) handleWebsocket(w http.ResponseWriter, r *http.Request, cphandler ChargePointMessageHandler) {

	cpID := strings.TrimPrefix(r.URL.Path, "/")

	if cpID == "" {
		log.Error("cpID is empty")
		return
	}

	fmt.Printf("\n===[SE HA CONECTADO: %s]===\n", cpID)

	PrintMemUsage()

	rawReq, _ := httputil.DumpRequest(r, true)
	fmt.Printf("\nRaw WS request: %s\n", string(rawReq))

	conn, err := ws.Handshake(w, r, []ocpp.Version{ocpp.V16})
	if err != nil {
		log.Error("Couldn't handshake request %w", err)
		return
	}

	// Cierra y elimina socket antiguo
	if csys.conns[cpID] != nil {

		csys.conns[cpID].Close()
		delete(csys.conns, cpID)
		// time.Sleep(500 * time.Millisecond)
		runtime.GC()

	}

	csys.conns[cpID] = conn

	fmt.Printf("\nConnected with %s \n", cpID)

	// Cuantos elementos en array de conexion
	fmt.Printf("\n Len of connections array: %d \n", len(csys.conns))
	ConnectedMachines = nil
	connectionsMutex.Lock()
	connections = csys.conns
	// csys.Mutex.Lock()
	// for position, connection := range csys.conns {
	for position, connection := range connections {
		fmt.Printf("\n Machine: %s, RemoteAddr: %s \n", position, connection.RemoteAddr())
		ConnectedMachines = append(ConnectedMachines, position)
	}
	connectionsMutex.Unlock()

	/* Manejador de Panics para handleWebsocket */
	defer func() {
		if err := recover(); err != nil {
			log.Debug("[handleWebsocketpanic] occurred:", err)
		} else {
			fmt.Printf("[%s] Cerrando hilo", cpID)
			debug.FreeOSMemory()
			runtime.GC()
		}
	}()

	go func() {
		/* Manejador de Panics para go func */
		defer func() {
			if err := recover(); err != nil {
				log.Debug("[go func] panic occurred:", err)
			}
		}()

		for {

			err := conn.ReadMessage(cpID)
			if err != nil {
				// log.Error("Error de lectura")
				/*MANEJO ORIGINAL DEL ERROR DE LECTURA*/
				/*
					log.Debug("Deleting connection of: %s", cpID)
					csys.conns[cpID].SetWriteDeadline(time.Now().Add(writeWait))
					csys.conns[cpID].WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
					time.Sleep(closeGracePeriod)

					if csys.conns[cpID] == nil {
						fmt.Println("No hay cpID")
						if !ws.IsNormalCloseError(err) {
							log.Error("On receiving a message: %w", err)
							// log.E("On receiving a message: %w", err)
						}
						break
					}

					// csys.conns[cpID].Read
					// fmt.Println("1 cpID: ", cpID)
					if _, ok := csys.conns[cpID]; ok {
						//do something here
						csys.conns[cpID].Close()
						// fmt.Println("2 cpID: ", cpID)
						time.Sleep(300 * time.Millisecond)
						delete(csys.conns, cpID)
					} else {
						fmt.Println("No hay cpID")
					}

					if !ws.IsNormalCloseError(err) {
						log.Error("On receiving a message: %w", err)
						// log.E("On receiving a message: %w", err)
					}
					// _ = conn.Close()
					RemoveMachine(cpID)
					// log.Debug("Closed connection of: %s", cpID)
					// delete(csys.conns, cpID)
				*/

				// /* Manejador de Panics */
				// defer func() {
				// 	if err := recover(); err != nil {
				// 		log.Debug("[if err != nil] panic occurred:", err)
				// 	}
				// }()

				// var antesEquipos []string
				// fmt.Println("Antes Conectados: ", len(csys.conns))
				// // csys.Mutex.Lock()
				// connectionsMutex.Lock()
				// connections = csys.conns
				// // csys.Mutex.Unlock()
				// // for position, _ := range csys.conns { // genera panics, como sea ... quizas hacer una copia y esa copia iterarla
				// for position, _ := range connections { // genera panics igual
				// 	antesEquipos = append(antesEquipos, position+", ")
				// }
				// connectionsMutex.Unlock()
				// fmt.Println(antesEquipos)

				if !ws.IsNormalCloseError(err) {
					log.Error(" %s [Abnormal Closure] [cs.go] On receiving a message: %s", cpID, err.Error())
				} else {
					log.Error(" %s [Normal Closure] On receiving a message: %s", cpID, err.Error())
				}
				err = conn.Close()
				if err != nil {
					log.Error("Error cerrando conexion: %s", err.Error())
				} else {
					log.Debug("Sin error Cierre de conexion")
					// done <- true
				}

				RemoveMachine(cpID)
				log.Debug("Closed connection of: %s", cpID)
				// time.Sleep(500 * time.Millisecond)

				// log.Debug("locking")
				delete(csys.conns, cpID)
				// time.Sleep(500 * time.Millisecond)
				// csys.Mutex.Unlock()

				// var despuesEquipos []string
				// fmt.Println("Ahora Conectados: ", len(csys.conns))

				// // csys.Mutex.Lock()
				// connectionsMutex.Lock()
				// connections = csys.conns
				// // csys.Mutex.Unlock()
				// // for position, _ := range csys.conns {
				// for position, _ := range connections {
				// 	// fmt.Println("Equipo", position)
				// 	despuesEquipos = append(despuesEquipos, position+", ")
				// }
				// connectionsMutex.Unlock()
				// csys.Mutex.Unlock()
				// log.Debug("Unlocking")
				// fmt.Println(despuesEquipos)
				break
			}

		}
	}()
	// defer close(conn.Requests())
	// var valor int

	// go func(valor int) {

	// 	}(valor)
	// defer close(conn.Requests())
	for {

		var llega time.Time
		fmt.Printf(".")

		req, kill := <-conn.Requests()
		if !kill {
			fmt.Println("Killing for loop")
			debug.FreeOSMemory()
			runtime.GC()
			break
		}
		if conn.RemoteAddr() == csys.conns[cpID].RemoteAddr() {
			fmt.Println("Socket identico OK")
			llega = time.Now()
			cprequest, ok := req.Request.(cpreq.ChargePointRequest)
			if !ok {
				log.Error(cpreq.ErrorNotChargePointRequest.Error())
			}
			cpresponse, err := cphandler(cprequest, cpID)
			err = conn.SendResponse(req.MessageID, cpresponse, err, cpID)
			if err != nil {
				log.Error(err.Error())
			}

		} else {
			fmt.Println("Socket diferente ... break")
			// csys.Mutex.Unlock()
			break
		}

		retorna := time.Now()
		diferencia := retorna.Sub(llega).Seconds()
		fmt.Printf("\n [for loop] Demora %f segundos en manejar mensaje\n", diferencia)
		fmt.Printf("-")
		// break // cierra hilo y no se puede volver a recibir mensajes
		// csys.Mutex.Unlock()

	}
}

func (csys *centralSystem) GetServiceOf(cpID string, version ocpp.Version, url string) (service.ChargePoint, error) {
	if version == ocpp.V15 {
		return service.NewChargePointSOAP(url, nil), nil
	}
	if version == ocpp.V16 {
		conn := csys.conns[cpID]
		if conn == nil { // TODO: or conn closed
			return nil, errors.New("no connection to this charge point")
		}
		return service.NewChargePointJSON(conn), nil
	}
	return nil, errors.New("charge point has no configured OCPP version(1.5/1.6)")
}

func (csys *centralSystem) DeleteConnection(cpID string) {
	fmt.Printf("Deleting %s connection", cpID)
	// cpID = "Charger123456"
	// csys.conns[cpID].Conn.Close()
	// csys.conns[cpID].Close()
	// csys.conns[cpID].Conn.CloseHandler()
	// csys.conns[cpID] = nil
	// csys.Mutex.Lock()
	csys.conns[cpID].SetReadDeadline(time.Now())
	csys.conns[cpID].Conn.Close()
	// delete(csys.conns, cpID)
	time.Sleep(500 * time.Millisecond)
	RemoveMachine(cpID)
	// csys.Mutex.Unlock()
	// csys.conns[cpID].SetWriteDeadline(time.Now().Add(writeWait))
	// csys.conns[cpID].WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	// time.Sleep(closeGracePeriod)
	// csys.conns[cpID].Close()

}

// PrintMemUsage outputs the current, total and OS memory being used. As well as the number
// of garage collection cycles completed.
func PrintMemUsage() uint64 {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGoroutines = %d", runtime.NumGoroutine())

	return bToMb(m.Alloc)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
